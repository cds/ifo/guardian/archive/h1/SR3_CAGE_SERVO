# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-
#
# $Id: $
# $HeadURL: $


import cdsutils as cdu
import time
from guardian import GuardState

nominal = 'CAGE_SERVO_OFF'

class CAGE_SERVO_OFF(GuardState):
    goto = True
    def main(self):
        self.wit = ezca['SUS-SR3_M3_WIT_PMON']
        return True
    def run(self):
        if self.wit == ezca['SUS-SR3_M3_WIT_PMON']:
            log('possible epics freeze')
            time.sleep(1)
        else:
            return True
        self.wit = ezca['SUS-SR3_M3_WIT_PMON']

class CAGE_SERVO_RUNNING(GuardState):

    control_chan = 'SUS-SR3_M1_DITHER_P_OFFSET'
    readback_chan = 'SUS-SR3_M3_OPLEV_PIT_OUTPUT' #'SUS-SR3_M3_WIT_PMON'
    servo_gain = 0.3 # ddb up gain a bit as wasn't keeping up #1 #0.01 # was 1, decreased by KI to achieve 3 min. control speed. Feb-5-2016
    servo_setpoint = 28.91 # was 28.98 DDB 26/3/23, then before that was 2.2 Dan Brown 2019-03-13 set for SR3 heater tests

    #985.26 # New val 07July KI, after a big EQ at Montana
    #servo_setpoint = 935.28 # New val 24May2017 JCD
    #929.1 New val 14Nov2016 JCD
    # setpoint was 931 until 14Nov2016 - big EQ moved things??
    #don't knwo why this was changed August 5 2016, undoing it for now  917.548
    # was 927, changed 2Mar2016 JCD # was 922, changed on 12 Feb 2016 JCD&NK
    def main(self):
        ezca.switch('SUS-SR3_M1_DITHER_P','OFFSET', 'ON')
        ezca['SUS-SR3_M1_DITHER_P_TRAMP']=0
        self.wit = ezca['SUS-SR3_M3_WIT_PMON']
        self.counter = 0
        if ezca['GRD-SUS_SR3_STATE_N'] == 100:
            self.not_aligned_flag = False
            # servo_setpoint = round(cdu.avg(5, 'SUS-SR3_M3_WIT_PMON'), 5)
            log(self.servo_setpoint)
            log(self.servo_gain)
            self.servo = cdu.Servo(ezca, self.control_chan, readback=self.readback_chan,
                               gain=self.servo_gain, setpoint=self.servo_setpoint)
        else:
            notify('SR3 not aligned!')
            self.not_aligned_flag = True

    def run(self):
        if self.counter > 16:
            log('reinitalizing epics servo because of epics freeze')
            self.servo = cdu.Servo(ezca, self.control_chan, readback=self.readback_chan,
                               gain=self.servo_gain, setpoint=self.servo_setpoint)
            self.counter = 0

        if abs(ezca['SUS-SR3_M1_DITHER_P_OUTPUT']) > 10000:
            notify('Align SR3 to offload M2 actuators')

        if ezca['GRD-SUS_SR3_STATE'] == 'ALIGNED' and not self.not_aligned_flag:
            #if the value has not changed (possibly an epics freeze) skip running the servo
            if self.wit == ezca['SUS-SR3_M3_WIT_PMON']:
                log('possible epics freeze')
                #keep track of how long we've possibly been frozen
                self.counter += 1
                return True
            else:
                self.counter =0 #reset epics freeze counter
                self.servo.step()
                self.wit = ezca['SUS-SR3_M3_WIT_PMON']
                return True
        else:
            notify('SR3 not aligned!')
            return 'CAGE_SERVO_OFF'

class CLEAR_OFFSET(GuardState):
    redirect = False
    def main(self):
        ezca['SUS-SR3_M1_DITHER_P_TRAMP']=30
        time.sleep(0.1)
        ezca['SUS-SR3_M1_DITHER_P_OFFSET']=0
        self.timer['ramp'] = ezca['SUS-SR3_M1_DITHER_P_TRAMP']
    def run(self):
        if self.timer['ramp']:
            return True

edges = [
        ('CAGE_SERVO_OFF', 'CAGE_SERVO_RUNNING'),
        ('CAGE_SERVO_RUNNING', 'CLEAR_OFFSET'),
        ('CAGE_SERVO_OFF', 'CLEAR_OFFSET')
        ]
